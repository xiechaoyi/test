﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Wlniao;
using Wlniao.XServer;

public partial class v1Controller : BaseController
{
    public override void OnActionExecuting(ActionExecutingContext filterContext)
    {
        ViewBag.Title = "重庆以名科技有限公司";
        ViewBag.Assets = "/eming_v1";
        ViewBag.ResVersion = "v=" + DateTools.GetUnix();
        base.OnActionExecuting(filterContext);
    }
}