﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Wlniao;
public partial class BaseController : XCoreController
{
    public IActionResult home()
    {
        return View();
    }
    public IActionResult about()
    {
        ViewBag.Title = "关于我们 - " + ViewBag.Title;
        return View();
    }
    public IActionResult product()
    {
        ViewBag.Title = "产品服务 - " + ViewBag.Title;
        return View();
    }
    public IActionResult caselist()
    {
        ViewBag.Title = "服务案例 - " + ViewBag.Title;
        return View();
    }
}