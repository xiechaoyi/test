FROM ccr.ccs.tencentyun.com/wlniao/dotnet:sdk-3.1 AS build
WORKDIR /tmp/build
COPY /. .
RUN dotnet publish -c Release -o /dist

FROM ccr.ccs.tencentyun.com/wlniao/dotnet:3.1.0
RUN rm -rf ./*
COPY --from=build /dist .
ENTRYPOINT ["dotnet", "app.dll"]