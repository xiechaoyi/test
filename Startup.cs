﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.DependencyInjection;

public class Startup
{
    public Startup(IWebHostEnvironment env)
    {

    }
    public void ConfigureServices(IServiceCollection services)
    {
        services.Configure<KestrelServerOptions>(options => { options.AllowSynchronousIO = true; });
        services.AddRazorPages().AddRazorRuntimeCompilation();
    }
    public void Configure(IApplicationBuilder app)
    {
        app.UseStaticFiles();
        app.UseRouting();
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllerRoute("Path", "{controller=v1}/{action=home}/{id?}");
            endpoints.MapRazorPages();
        });
    }
}